﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Mi9WebApi.Models
{
    public class Response
    {
        [DataMember]
        public string Image { get; set; }
        [DataMember]
        public string Slug { get; set; }
        [DataMember]
        public string Title { get; set; }
    }
}